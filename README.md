# OpenML dataset: New-York-City-Bike-Share-Dataset

https://www.openml.org/d/43526

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

The New York City Bike Share enables quick, easy, and affordable bike trips around the New York city boroughs. They make regular open data releases (this dataset is a transformed version of the data from this link). The dataset contains 735502 anonymised trips information made from Jan 2015 to June 2017.
Acknowledgements -
This dataset is the property of NYC Bike Share, LLC and Jersey City Bike Share, LLC (Bikeshare) operates New York Citys Citi Bike bicycle sharing service for TC click here
Objectives -

EDA
Feature Engineering
Predict Gender of the riders

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43526) of an [OpenML dataset](https://www.openml.org/d/43526). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43526/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43526/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43526/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

